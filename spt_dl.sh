#!/usr/bin/env bash

CLIENT_ID=$(jq -r ".client_id" config.json)
CLIENT_SECRET=$(jq -r ".client_secret" config.json)
SPT_USER="emericavega"
TOKEN_URL="https://accounts.spotify.com/api/token"
API_URL="https://api.spotify.com/v1"
YT_URL="https://www.youtube.com"

# metadata mp3 source https://write.corbpie.com/adding-metadata-to-a-video-or-audio-file-with-ffmpeg/
get_token () {
  creds=$(echo -n "$CLIENT_ID:$CLIENT_SECRET" | base64 | tr -d "\n")
  result=$(curl -s -X POST -H "Authorization: Basic $creds" \
    -d grant_type=client_credentials $TOKEN_URL)
  token=$(echo "$result" | jq -r ".access_token")
}

call_api () {
  endpoint=$1
	result=$(curl -s "$API_URL/$endpoint" \
		-H "Authorization: Bearer $token" \
		-H "Content-Type: application/json")
  #test=$(echo "$result" | jq '.error')
  test=$(jq '.error' <<< "$result")
  if [ -n "$test" ]; then
    get_token
    result=$(curl -s "$API_URL/$endpoint" \
      -H "Authorization: Bearer $token" \
      -H "Content-Type: application/json")
  fi
}

call_api "users/$SPT_USER/playlists"
echo "$result" | jq -c -r '.items[]' | while read -r i; do
  playid=$(jq -r '.id' <<< "$i")
  playlistname=$(jq -r '.name' <<< "$i")
  echo "$playlistname"
  mkdir -p "$HOME/Musik/$playlistname"
  call_api "playlists/$playid/tracks?market=DE"
  tracks="$result"
  len=$(jq ".items | length" <<< "$tracks")
  len=$((len - 1))
  for i in $(seq 0 $len)
  do
    unset metadata
    track=$(jq ".items[$i].track" <<< "$tracks")
    declare -A metadata
    title=$(jq -r '.name' <<< "$track")
    metadata[title]=$title
    artists=$(jq -r '.artists | map(.name) | join(", ")' <<< "$track")
    metadata[artist]=$artists
    # Genre aller Artists holen
    artists_ids=$(jq -r '.artists[] | .id' <<< "$track")
    for a in $artists_ids
    do
      call_api "artists/$a"
      genres_all=""
      genres=$(jq -r '.genres[]' <<< "$result")
      for g in $genres
      do
        genres_all+="${g} / "
      done
      if [ -n "$genres_all" ]; then
        genres=${genres_all::-2}
      else
        genres=""
      fi
    done
    metadata[genres]=$genres
    album=$(jq -r '.album.name' <<< "$track")
    metadata[album]=$album
    date=$(jq -r '.album | .release_date' <<< "$track")
    metadata[date]=$date
    album_artist=$(jq -r '.album.artists | map(.name) | join(", ")' <<< "$track")
    metadata[album_artist]=$album_artist
    # get album cover
    cover_url=$(jq -r '.album.images[1].url' <<< "$track")
    curl -s "$cover_url" > cover.jpg
    for key in "${!metadata[@]}"
    do 
      printf "%s => ${metadata[$key]}\n" "$key"
    done
    query=$(echo "$track" | jq -r '[.artists[].name, .name] | join(" ")' | sed 's/ /+/g')
    echo "$YT_URL/results?search_query=$query"
    id=$(curl -s "$YT_URL/results?search_query=$query" | \
    	grep -Eo "\"videoId\":\"[A-Za-z0-9]+\"" | \
    	head -n1 | cut -d":" -f2 | tr -d '"')
    #youtube-dl -f m4a "$YT_URL/watch?v=$id" -o "$title.m4a"
    yt-dlp -f m4a "$YT_URL/watch?v=$id" -o "$title.m4a"
    ffmpeg  -nostdin -i "$title.m4a" -i cover.jpg -map 0:0 -map 1:0 -c:a libmp3lame -q:a 4 \
      -metadata title="${metadata[title]}" \
      -metadata artist="${metadata[artist]}" \
      -metadata genre="${metadata[genres]}" \
      -metadata album="${metadata[album]}" \
      -metadata date="${metadata[date]}" \
      -metadata album_artist="${metadata[album_artist]}" \
      "$HOME/Musik/$playlistname/$title.mp3"
  done
done
